import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { PageEvent, MatPaginator } from '@angular/material/paginator';
import { User } from './common/interfaces';
import { Moviiservice } from './common/interfaces'; /*oksi*/
import { ApiService } from './services/api.service';
import {MatTableDataSource} from '@angular/material/table';
import { HttpClient } from "@angular/common/http";
import moviiservices  from './_files/moviiservices.json';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  //title = 'Angular Example'; /**/
  //products: any = []; /** */
  httpClient: HttpClient /** */
    servicitos: any = moviiservices;
    //datata: {code:string; name:string}[]=moviiservices; /** */
    //public countryList:{name:string, code:string}[] = countries;

  constructor(
    private api: ApiService,
    httpClient: HttpClient /** */
  ){}

  ngOnInit(){
    this.api.getLista().subscribe(datos => this.datos = datos);

    this.httpClient.get("./_files/moviiservices.json").subscribe(datosmoviiservice =>{
      console.log(datosmoviiservice);
      //this.products = datosmoviiservice;
    }) 
    /* this.httpClient.get("assets/data.json").subscribe(data =>{
      console.log(data);
      this.products = data;
    }) */
  }
  datosmoviiservice: Moviiservice[] = []
  datos: User[] = [] //¿tamaño total de objetos?

  handlePage(e: PageEvent){
    this.page_size = e.pageSize
    this.page_number = e.pageIndex + 1
  }

  page_size: number = 5 // CANTIDAD DE ELEMENTOS POR PAGINA
  page_number: number = 1
  pageSizeOptions = [5, 10, 20, 50, 100]
}
