import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { PageEvent, MatPaginator } from '@angular/material/paginator';
import { HttpClient } from "@angular/common/http";
import moviiservices  from './services/moviiservices.json';
import result  from './services/result.json';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  httpClient: HttpClient;
  servicios: any = moviiservices;
  resultado: any = result;
  
  constructor(
    httpClient: HttpClient
  ){}

  ngOnInit(){ 
    // Carga los json de servicios de consulta y resultados a listar en la tabla
    this.httpClient.get("./services/moviiservices.json").subscribe(datosmoviiservice =>{console.log(datosmoviiservice);});
    this.httpClient.get("./services/result.json").subscribe(result =>{console.log(result);});
  }

  //Paginación
  handlePage(e: PageEvent){
    this.page_size = e.pageSize
    this.page_number = e.pageIndex + 1
  }

  page_size: number = 5 
  page_number: number = 1
  pageSizeOptions = [5, 10, 20, 50, 100]
}
